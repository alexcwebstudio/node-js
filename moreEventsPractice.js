var events = require("events");
var eventEmitter = new events.EventEmitter();

//making listener 1
var listener1 = function listener1() {
	console.log("This is listener one");
}

 //stating that listener 1 is a thing
 eventEmitter.addListener('connection', listener1);

//setting off the events emmitter(listener1) based off calling 'connection'
eventEmitter.emit('connection');
