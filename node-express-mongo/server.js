console.log('this is the server side');

const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const app = express();

//serving files from local dir
app.use(express.static('public'));

//connect to the db and start the express server
let db;

//adding connection to db
const url = 'mongodb://127.0.0.1:29783/clicks';

MongoClient.connect(url, (err, database) => {
	if (err) {
		return console.log('err')
	}
	db = database;
	//starting server on port 8080
	app.listen(8080, () => {
		console.log('listening on port 8080');
	});
});

//serve the homepage
app.get('/', (req, res) =>{
	res.sendFile(__dirname+'/index.html');
});

//adding a created document to the data base collection
app.post('/clicked', (req, res)=>{
	const click = {clickTime: new Date()};
	console.log(click);
	console.log(db);

	db.collection('clicks').save(click, (err, result) => {
		if (err) {
			return console.log(err);
		}
		console.log('click added to db');
		res.sendStatus(201);
	});
});

//getting the data from the db to show it
app.get('/clicks', (req, res) => {
  db.collection('clicks').find().toArray((err, result) => {
    if (err) return console.log(err);
    res.send(result);
  });
});






