// importing eventd module
var events = require('events');

//creating a new eventEmitter object
var eventEmitter = new events.EventEmitter();

//setting up handler named connectionHandler
var connectionHandler = function connected() {
	console.log("the connection is good")
}

//on event = connection do the connectionHandler function
eventEmitter.on('connection', connectionHandler);

//calling the connection event 
eventEmitter.emit('connection');

console.log("the program is done")

