// This is a blocking example
//setting up the file system
var fs = require('fs');
//creating data location
var data = fs.readFileSync('input.txt');

//using to string to make sure if there is numbers in the file it 
//will read as a string
console.log(data.toString());
console.log("end of the program");
