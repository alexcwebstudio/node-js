//setting up express
var express = require('express');
var app = express();

//require path for file system
var path = require('path');

//express function for request and response
//serves this on response
app.get('/', function(req, res){
	//on response send file at a defined directtory name
	//and the file would be index.html
	res.sendFile(path.join(__dirname+'/index.html'));
});

//setting what port it will post on
app.listen(3000);
console.log("server running at Port 3000")


