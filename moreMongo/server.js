console.log("this is the server")

//setting up imports for the app
const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const app = express();

let db;

//adding connection to db
const url = 'mongodb://127.0.0.1:29783/clicks';

MongoClient.connect(url,  { useNewUrlParser: true }, (err, database) => {
	if (err) {
		return console.log('err')
	}
	db = database;
	//starting server on port 8080
	app.listen(8080, () => {
		console.log('listening on port 8080');
	});
});

//serving the home page
app.get('/', (req, res) => {
	res.sendFile(__dirname+'/public/index.html');
});