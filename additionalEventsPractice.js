//calling events
var events = require('events');
var eventEmitter = new events.EventEmitter();

//crating the handler
var myEventHandler = function() {
	console.log('I hear a scream');
}

//Assigning the event handler to an event
eventEmitter.on('scream', myEventHandler);

//Fire off the 'scream' event
eventEmitter.emit('scream');

